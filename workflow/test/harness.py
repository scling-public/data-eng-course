import json
import logging
import os
import shutil
import subprocess
import sys
from contextlib import closing
from pathlib import Path
# noinspection PyUnresolvedReferences
from unittest import TestCase, skip

import luigi
import mysql.connector
from luigi.cmdline_parser import CmdlineParser
from retrying import retry

from lib.coordinates import LUIGI, PYTHONPATH, PATH, INTERNAL_LUIGI, TEST_LAKE, MYSQL_DOCKER, MYSQL_HOST, DOCKER


class WorkflowTestCase(TestCase):
    def setUp(self):
        if Path(TEST_LAKE).exists():
            shutil.rmtree(TEST_LAKE)


def write_dataset(path, data):
    raw = '\n'.join([json.dumps(j) for j in data] + [''])
    write_raw_dataset(path, raw)


def write_raw_dataset(path, raw):
    if path.exists():
        shutil.rmtree(str(path))
    path.mkdir(parents=True)
    (path / 'part-00000.json').write_text(raw)
    (path / '_SUCCESS').write_text('')
    print(f'Wrote test input dataset, {len(raw.splitlines())} lines to {path}')


def read_dataset(path):
    if (path / '_SUCCESS').exists():
        return [json.loads(line)
                for data_file in path.glob('*.json')
                for line in data_file.read_text().splitlines()]


def run_luigi(module, job, **kwargs):
    luigi_args = (['--local-scheduler', '--module', module, job] +
                  [f'--{k.replace("_", "-")}={v}' for k, v in kwargs.items()])
    os.environ['PATH'] = os.pathsep.join(os.environ.get('PATH', '').split(os.pathsep) + PATH)
    if not INTERNAL_LUIGI:
        env = dict(os.environ)
        python_path = ':'.join(env.get('PYTHONPATH', '').split(':') + PYTHONPATH)
        env['PYTHONPATH'] = python_path
        luigi_cmd = [LUIGI] + luigi_args
        print('> ' + ' '.join(luigi_cmd))
        subprocess.check_call(luigi_cmd, env=env)
    else:
        sys.path.extend(PYTHONPATH)
        print('Running internal luigi, args: ' + ' '.join(luigi_args))
        with CmdlineParser.global_instance(luigi_args) as parser:
            luigi.build([parser.get_task_obj()])


def retry_connection_refused(e: Exception):
    return isinstance(e, mysql.connector.errors.DatabaseError) and e.msg.find("Can't connect") != -1


class DbWorkflowTestcase(WorkflowTestCase):
    db_container_name = 'tiny-test-db'
    db_password = 'tiny-test'
    db_host = None

    @classmethod
    def setUpClass(cls) -> None:
        logging.basicConfig(level=logging.DEBUG)
        if MYSQL_DOCKER:
            cls.db_host = cls.find_db()
            if cls.db_host is None:
                cls.startDb()
                cls.db_host = cls.find_db()
                assert (cls.db_host)
        else:
            cls.db_host = MYSQL_HOST

    def setUp(self):
        super(DbWorkflowTestcase, self).setUp()
        self.db_execute('drop table if exists table_updates')

    @classmethod
    def find_db(cls):
        try:
            inspect_out = subprocess.check_output([DOCKER, 'inspect', cls.db_container_name],
                                                  universal_newlines=True)
            inspect = json.loads(inspect_out)
            if inspect[0]['State']['Status'] != 'running':
                logging.info('Found dead test DB container')
                return None
            logging.info('Found healthy test DB container')
            return inspect[0]['NetworkSettings']['IPAddress']
        except Exception as e:
            logging.debug(f'Could not find DB container, need to start one: {e}')

    @classmethod
    def startDb(cls):
        subprocess.run([DOCKER, 'rm', cls.db_container_name])
        subprocess.run([DOCKER, 'run',
                        '--name', cls.db_container_name,
                        '--env', f'MYSQL_USER=tiny',
                        '--env', f'MYSQL_PASSWORD={cls.db_password}',
                        '--env', f'MYSQL_RANDOM_ROOT_PASSWORD=1',
                        '--env', f'MYSQL_DATABASE=tinydb',
                        '--detach',
                        'mysql:8.0'])

    @classmethod
    def tearDownClass(cls) -> None:
        # Leave the container running for faster testing.
        # subprocess.run([DOCKER, 'kill', cls.db_container_name])
        # cls.db_container_name = None
        cls.db_host = None

    @classmethod
    def db_execute(cls, query: str, extract=lambda c: []):
        logging.info(f'> {query}')
        with closing(cls.db_connect()) as db:
            with closing(db.cursor(dictionary=True)) as cursor:
                logging.info(query)
                cursor.execute(query)
                return extract(cursor)

    @classmethod
    @retry(retry_on_exception=retry_connection_refused, wait_fixed=1000, stop_max_delay=20000)
    def db_connect(cls):
        logging.debug('Attempting to connect to test db')
        conn = mysql.connector.connect(host=cls.db_host, user='tiny', password=cls.db_password, db='tinydb',
                                       autocommit=True)
        return conn

    def get_db_rows(self, table):
        results = self.db_execute(f"SELECT * FROM {table}", lambda c: list(c.fetchall()))
        logging.debug(f"DB contents: {results}")
        return results
