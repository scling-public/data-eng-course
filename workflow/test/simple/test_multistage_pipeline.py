# noinspection PyUnresolvedReferences
import json
from pathlib import Path
# noinspection PyUnresolvedReferences
from unittest import TestCase, skip

from lib.coordinates import TEST_LAKE
from test.harness import run_luigi, write_dataset, read_dataset, WorkflowTestCase


class MultiStageTest(WorkflowTestCase):
    def run_ingest(self, h):
        run_luigi('exercises.simple.multistage', 'PageviewCopy', hour=h)

    def run_dedup(self, h):
        run_luigi('exercises.simple.multistage', 'PageviewDedup', hour=h)

    @skip("Remove this line to enable the test")
    def test_happy(self):
        ingest_08_path = Path(f'{TEST_LAKE}/ingest/Pageview/year=2019/month=10/day=13/hour=08')
        ingest_08 = [{'url': 'https://www.acme.com/', 'time': '2019-10-13T08:01:02.492743Z',
                      'id': '123e4567-e89b-12d3-a456-426655440000'}]
        write_dataset(ingest_08_path, ingest_08)

        self.run_ingest('2019-10-13T08')

        cold_path = Path(f'{TEST_LAKE}/cold/PageviewHourly/year=2019/month=10/day=13/hour=08')
        cold_08 = read_dataset(cold_path)
        self.assertCountEqual(ingest_08, cold_08)

        ingest_09_path = Path(f'{TEST_LAKE}/ingest/Pageview/year=2019/month=10/day=13/hour=09')
        ingest_09 = [{'url': 'https://www.acme.com/', 'time': '2019-10-13T08:01:02.492999Z',
                      'id': '123e4567-e89b-12d3-a456-426655440000'},
                     {'url': 'https://www.acme.com/about', 'time': '2019-10-13T09:02:03.492743Z',
                      'id': 'ff3e4567-e89b-12d3-a456-426655440000'}]
        write_dataset(ingest_09_path, ingest_09)

        # Ingest should be scheduled separately. Hence, this is not supposed to run the ingest step for hour 09.
        self.run_dedup('2019-10-13T09')
        # The job is supposed to have failed with missing external data dependencies
        dedup_09_path = Path(f'{TEST_LAKE}/derived/PageviewDedup/year=2019/month=10/day=13/hour=09')
        dedup_empty = read_dataset(dedup_09_path)
        self.assertIsNone(dedup_empty,
                          msg="At this point, the deduplication step should have reported missing external "
                              "dependencies, but your pipeline seems to have executed the ingestion prematurely.")

        # Now, explicitly run the ingestion.
        self.run_ingest('2019-10-13T09')
        self.run_dedup('2019-10-13T09')
        deduped = read_dataset(dedup_09_path)
        self.assertCountEqual(ingest_09[1:], deduped)
