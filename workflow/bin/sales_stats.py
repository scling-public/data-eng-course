#! /usr/bin/env python3

import argparse
import sys
from pathlib import Path

from lib.dataset import read_json_lines, write_json_lines


"""Trivial data processing script. In a real scenario, some meaningful processing would happen here. But the purpose 
of the exercise is to learn workflows, so this tiny script merely performs a simple aggregation."""

def main(argv):
    parser = argparse.ArgumentParser()
    # Accept the superset of arguments used in the exercises.
    parser.add_argument('--date')
    parser.add_argument('--input')
    parser.add_argument('--output')
    args = parser.parse_args(argv)

    sales = read_json_lines(Path(args.input))
    output = [{
        'date': args.date,
        'num_items': len(set([r['item'] for r in sales])),
        'num_users': len(set([r['user'] for r in sales])),
    }]

    write_json_lines(Path(args.output), output)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
