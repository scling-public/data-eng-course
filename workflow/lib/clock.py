import logging
from datetime import datetime, timedelta


class Clock:
    # Override this when testing.
    _current_time = None

    @classmethod
    def now(cls):
        if cls._current_time:
            now = cls._current_time
            # Logic might make a reasonable assumption that time eventually moves forward.
            cls._current_time += timedelta(microseconds=1)
            return now
        return datetime.now()

    @classmethod
    def set_time(cls, t):
        cls._current_time = t
        logging.info(f'Current workflow time set to {cls._current_time}')
