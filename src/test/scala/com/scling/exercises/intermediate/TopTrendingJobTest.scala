package com.scling.exercises.intermediate

import better.files.File
import com.scling.exercises._
import org.scalatest.{FunSuite, Ignore}

class TopTrendingJobTest extends FunSuite with BatchJobTestRunner2[Order, Order, TrendingItem] {
  import Serde._
  import TestInput._

  implicit val trendingItemOrdering: Ordering[TrendingItem] = Ordering.by(TrendingItem.unapply)

  override def runJob(yesterday: File, today: File, output: File): Unit =
    TopTrendingJob.main(Array("TopTrending", "3", yesterday.toString, today.toString, output.toString))

  test("Empty input") {
    assert(runTest(Seq(), Seq()) === Seq())
  }

  ignore("Empty yesterday") {
    assert(runTest(Seq(), Seq(Order("1", "alice", "pants"))) === Seq())
  }

  ignore("Empty today") {
    assert(runTest(Seq(Order("1", "alice", "pants")), Seq()) === Seq(TrendingItem("pants", 1, -100)))
  }

  ignore("One unchanged") {
    assert(runTest(Seq(Order("1", "alice", "pants")), Seq(Order("11", "alice", "pants"))) ===
      Seq(TrendingItem("pants", 1, 0)))
  }

  ignore("Two increased") {
    assert(runTest(
      orders(1, "alice", "pants") ++
        orders(1, "cecilia", "jumper"),
      orders(1, "alice", "pants") ++
        orders(1, "bob", "pants") ++
        orders(2, "cecilia", "jumper") ++
        orders(1, "Denise", "jumper"))
      ===
      Seq(
        TrendingItem("jumper", 1, 200),
        TrendingItem("pants", 2, 100)
      ))
  }

  ignore("One gone") {
    assert(runTest(
      orders(1, "alice", "pants") ++
        orders(1, "cecilia", "jumper"),
      orders(2, "cecilia", "jumper") ++
        orders(1, "Denise", "jumper"))
      ===
      Seq(
        TrendingItem("jumper", 1, 200),
        TrendingItem("pants", 2, -100)
      ))
  }

  ignore("One same, one increased, one new") {
    assert(runTest(
      orders(1, "alice", "pants") ++
        orders(1, "cecilia", "jumper"),
      orders(1, "alice", "pants") ++
        orders(1, "bob", "socks") ++
        orders(2, "cecilia", "jumper") ++
        orders(1, "Denise", "jumper"))
      ===
      Seq(
        TrendingItem("jumper", 1, 200),
        TrendingItem("pants", 2, 0)
      ))
  }

  ignore("Top list cut off") {
    assert(runTest(
      orders(2, "alice", "pants") ++
        orders(1, "cecilia", "jumper") ++
        orders(4, "cecilia", "skirt") ++
        orders(1, "bob", "gloves"),
      orders(3, "alice", "pants") ++
        orders(3, "bob", "gloves") ++
        orders(5, "cecilia", "skirt") ++
        orders(2, "cecilia", "jumper"))
      ===
      Seq(
        TrendingItem("gloves", 1, 200),
        TrendingItem("jumper", 2, 100),
        TrendingItem("pants", 3, 50)
      ))
  }
}
