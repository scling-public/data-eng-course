#!/usr/bin/env bash

set -eux

# shellcheck disable=SC2046
cd $(dirname $0)/..

git checkout solutions
./gradlew testAll
git checkout master
./gradlew testAll
./gradlew clean
